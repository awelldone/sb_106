After https://www.tutorialspoint.com/spring/spring_applicationcontext_container.htm


MainApp.java | Beans.XML / Fiz.xml, Ds-test.xml 

MainApp
- aContext = Beans.xml
-      obj = aContext.getBean("hellow")
-      obj.getMessage()


Beans.XML
- <import resource="Ds-dev.xml"
- <import resource="Ds-test.xml"
- <import resource="Hellow.xml"
- <import resource="Fiz.xml"

Ds-test.xml
   <beans xmlns = "... ">
     <bean id = "fizDAO" class = "com.example.JdbcFizDAO"> 
          <property name = "dataSource" ref = "dataSource!"/>
          <bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer"></bean>             
  
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
          <property name="driverClassName" value="org.postgresql.Driver" />
          <property name="url"             value="jdbc:postgresql://fizdburl.hu/fizdbname" />
          <property name="username"        value="fizdbuser" />
          <property name="password"        value="${teszt.database.password}" />
          
Hellow.xml
   <beans xmlns = "... ">
      <bean  id = "helloW" class = "com.tutorialspoint.HelloW"
           <property name = "message" value = "Hello World! "/>

HelloW.java
    private String message; with setter

Fiz.xml
   <beans xmlns = "... ">
      <bean  id = "fizDao" class = "com.tutorialspoint.FizDao"
           <property name = "dataSource" value = "dataSource!"/>

JdbcFizDAO.java
      private DataSource dataSource; with setter





